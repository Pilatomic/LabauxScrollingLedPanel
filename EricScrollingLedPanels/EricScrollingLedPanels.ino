
#include <DIO2.h>  // include the fast I/O 2 functions

const uint8_t pictureR[] PROGMEM = {
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0x3, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 0x70, 0xff, 0x9f, 0xff, 0xfe, 0x7f, 0xf9,
0xfe, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xff, 0x0, 0xff,
0xfc, 0xff, 0x7, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xe7, 0xff, 0xff, 0xf8, 0x3f, 0xf3, 0xfc,
0x7f, 0xff, 0xff, 0xff, 0xff, 0xe7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf8, 0xc, 0x7f,
0x9f, 0xff, 0xfe, 0x7f, 0xf9, 0xfe, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x7f,
0xff, 0xff, 0xff, 0x0, 0xff, 0xfc, 0xff, 0x7, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xe7, 0xff,
0xff, 0xf8, 0x3f, 0xf3, 0xfc, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xe7, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xf3, 0x7, 0x3f, 0x9f, 0xff, 0xfe, 0x7f, 0xf9, 0xfe, 0x1f, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xff, 0x3f, 0xff, 0xfc, 0xff, 0xe7, 0xff, 0xfe,
0x7f, 0xff, 0xff, 0xe7, 0xff, 0xff, 0xff, 0x3f, 0xf3, 0xfc, 0x3f, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf7, 0x3, 0xff, 0x9f, 0xff, 0xfe, 0x7f, 0xf9,
0xfe, 0x9f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xff, 0x3f, 0xff,
0xfc, 0xff, 0xe7, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xe7, 0xff, 0xff, 0xff, 0x3f, 0xf3, 0xfd,
0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe7, 0x87, 0xff,
0x9f, 0xf8, 0x3e, 0x47, 0xf9, 0xfc, 0x9f, 0x3c, 0xcf, 0x9f, 0xff, 0xf3, 0xff, 0xfe, 0x7f,
0xf0, 0xff, 0xff, 0x3f, 0xf0, 0x7c, 0x8f, 0xe7, 0xf8, 0x3e, 0x47, 0xff, 0xfc, 0x27, 0xc3,
0xff, 0xff, 0x3f, 0xf3, 0xf9, 0x3e, 0x79, 0x9f, 0x3c, 0x1f, 0x7, 0xf0, 0x7f, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xe7, 0xff, 0xff, 0x9f, 0xf0, 0x1e, 0x3, 0xff, 0xfc, 0xcf, 0x3c, 0xe7, 0x3f,
0xff, 0xf3, 0xff, 0xfe, 0x7f, 0xc0, 0x7f, 0xff, 0x3f, 0xe0, 0x3c, 0x7, 0xe7, 0xf0, 0x1e,
0x3, 0xff, 0xf8, 0x7, 0x1, 0xff, 0xff, 0x3f, 0xff, 0xf9, 0x9e, 0x79, 0xce, 0x78, 0xf,
0x7, 0xe0, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe4, 0xff, 0xff, 0x9f, 0xf7, 0xce, 0x79, 0xff,
0xfd, 0xcf, 0x3c, 0xf3, 0x7f, 0xff, 0xf3, 0xff, 0xfe, 0x7f, 0xcf, 0x3f, 0xff, 0x0, 0xef,
0x9c, 0xf3, 0xe7, 0xf7, 0xce, 0x79, 0xff, 0xf9, 0xc7, 0x3c, 0xff, 0xff, 0x3f, 0xff, 0xfb,
0x9e, 0x79, 0xe6, 0xf9, 0xc7, 0xe7, 0xe7, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe4, 0x7f, 0xff,
0x9f, 0xf8, 0xe, 0x79, 0xff, 0xf9, 0xcf, 0x3c, 0xf8, 0x7f, 0xff, 0xff, 0xff, 0xfe, 0x7f,
0x9f, 0xbf, 0xff, 0x0, 0xf0, 0x1c, 0xf3, 0xe7, 0xf8, 0xe, 0x79, 0xff, 0xf3, 0xe6, 0x7e,
0xff, 0xff, 0x3f, 0xff, 0xf3, 0x9e, 0x79, 0xf0, 0xf3, 0xe7, 0xe7, 0xe7, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xe4, 0x3f, 0xff, 0x9f, 0xf0, 0xe, 0x79, 0xff, 0xf9, 0xef, 0x3c, 0xfc, 0xff,
0xff, 0xff, 0xff, 0xfe, 0x7f, 0x80, 0x3f, 0xff, 0x3f, 0xe0, 0x1c, 0xf3, 0xe7, 0xf0, 0xe,
0x79, 0xff, 0xf3, 0xe6, 0x0, 0xff, 0xff, 0x3f, 0xff, 0xf3, 0xde, 0x79, 0xf9, 0xf3, 0xe7,
0xe7, 0xf1, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe4, 0x3f, 0xff, 0x9f, 0xe3, 0xce, 0x79, 0xff,
0xf8, 0x7, 0x3c, 0xf8, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x7f, 0x80, 0x3f, 0xff, 0x3f, 0xc7,
0x9c, 0xf3, 0xe7, 0xe3, 0xce, 0x79, 0xff, 0xf3, 0xe6, 0x0, 0xff, 0xff, 0x3f, 0xff, 0xf0,
0xe, 0x79, 0xf1, 0xf3, 0xe7, 0xe7, 0xfe, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf6, 0x3f, 0xff,
0x9f, 0xe7, 0xce, 0x79, 0xff, 0xf0, 0x7, 0x3c, 0xf2, 0x7f, 0xff, 0xff, 0xff, 0xfe, 0x7f,
0x9f, 0xff, 0xff, 0x3f, 0xcf, 0x9c, 0xf3, 0xe7, 0xe7, 0xce, 0x79, 0xff, 0xf3, 0xe6, 0x7f,
0xff, 0xff, 0x3f, 0xff, 0xe0, 0xe, 0x79, 0xe4, 0xf3, 0xe7, 0xe7, 0xff, 0xbf, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xf2, 0x7f, 0xff, 0x9f, 0xe7, 0x8e, 0x79, 0xff, 0xf3, 0xe7, 0x38, 0xf3, 0x3f,
0xff, 0xf3, 0xff, 0xfe, 0x7f, 0xcf, 0xbf, 0xff, 0x3f, 0xcf, 0x1c, 0xf3, 0xe7, 0xe7, 0x8e,
0x79, 0xff, 0xf9, 0xc7, 0x3e, 0xff, 0xff, 0x3f, 0xff, 0xe7, 0xce, 0x71, 0xe6, 0x79, 0xc7,
0xe7, 0xef, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf8, 0x7f, 0xff, 0x80, 0x30, 0xe, 0x3, 0xff,
0xf3, 0xf3, 0x0, 0xe7, 0x9f, 0xff, 0xf3, 0xff, 0xfe, 0x0, 0xc0, 0x3f, 0xff, 0x3f, 0xe0,
0x1c, 0x7, 0xf0, 0x70, 0xe, 0x3, 0xff, 0xf8, 0x7, 0x0, 0xff, 0xff, 0x83, 0xff, 0xe7,
0xe6, 0x1, 0xcf, 0x38, 0xf, 0x0, 0xe0, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 0x3f, 0xff,
0x80, 0x38, 0x4e, 0x47, 0xff, 0xe7, 0xf3, 0x84, 0xcf, 0x9f, 0xff, 0xf3, 0xff, 0xfe, 0x0,
0xf0, 0x7f, 0xff, 0x3f, 0xf0, 0x9c, 0x8f, 0xf8, 0x78, 0x4e, 0x47, 0xff, 0xfc, 0x27, 0xc1,
0xff, 0xff, 0xc3, 0xff, 0xcf, 0xe7, 0x9, 0x9f, 0x3c, 0x1f, 0x0, 0xf0, 0x7f, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff,
 };

const uint8_t pictureG[] PROGMEM = {
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x9f, 0xff, 0xfe, 0x7f, 0xf9,
0xfe, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xff, 0x0, 0xff,
0xfc, 0xff, 0x7, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xe7, 0xff, 0xff, 0xf8, 0x3f, 0xf3, 0xfc,
0x7f, 0xff, 0xff, 0xff, 0xff, 0xe7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0x9f, 0xff, 0xfe, 0x7f, 0xf9, 0xfe, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x7f,
0xff, 0xff, 0xff, 0x0, 0xff, 0xfc, 0xff, 0x7, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xe7, 0xff,
0xff, 0xf8, 0x3f, 0xf3, 0xfc, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xe7, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0x9f, 0xff, 0xfe, 0x7f, 0xf9, 0xfe, 0x1f, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xff, 0x3f, 0xff, 0xfc, 0xff, 0xe7, 0xff, 0xfe,
0x7f, 0xff, 0xff, 0xe7, 0xff, 0xff, 0xff, 0x3f, 0xf3, 0xfc, 0x3f, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf7, 0xff, 0xbf, 0x9f, 0xff, 0xfe, 0x7f, 0xf9,
0xfe, 0x9f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xff, 0x3f, 0xff,
0xfc, 0xff, 0xe7, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xe7, 0xff, 0xff, 0xff, 0x3f, 0xf3, 0xfd,
0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe7, 0xff, 0x9f,
0x9f, 0xf8, 0x3e, 0x47, 0xf9, 0xfc, 0x9f, 0x3c, 0xcf, 0x9f, 0xff, 0xf3, 0xff, 0xfe, 0x7f,
0xf0, 0xff, 0xff, 0x3f, 0xf0, 0x7c, 0x8f, 0xe7, 0xf8, 0x3e, 0x47, 0xff, 0xfc, 0x27, 0xc3,
0xff, 0xff, 0x3f, 0xf3, 0xf9, 0x3e, 0x79, 0x9f, 0x3c, 0x1f, 0x7, 0xf0, 0x7f, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xe7, 0xff, 0x9f, 0x9f, 0xf0, 0x1e, 0x3, 0xff, 0xfc, 0xcf, 0x3c, 0xe7, 0x3f,
0xff, 0xf3, 0xff, 0xfe, 0x7f, 0xc0, 0x7f, 0xff, 0x3f, 0xe0, 0x3c, 0x7, 0xe7, 0xf0, 0x1e,
0x3, 0xff, 0xf8, 0x7, 0x1, 0xff, 0xff, 0x3f, 0xff, 0xf9, 0x9e, 0x79, 0xce, 0x78, 0xf,
0x7, 0xe0, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe4, 0xfc, 0x1f, 0x9f, 0xf7, 0xce, 0x79, 0xff,
0xfd, 0xcf, 0x3c, 0xf3, 0x7f, 0xff, 0xf3, 0xff, 0xfe, 0x7f, 0xcf, 0x3f, 0xff, 0x0, 0xef,
0x9c, 0xf3, 0xe7, 0xf7, 0xce, 0x79, 0xff, 0xf9, 0xc7, 0x3c, 0xff, 0xff, 0x3f, 0xff, 0xfb,
0x9e, 0x79, 0xe6, 0xf9, 0xc7, 0xe7, 0xe7, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe4, 0x78, 0x1f,
0x9f, 0xf8, 0xe, 0x79, 0xff, 0xf9, 0xcf, 0x3c, 0xf8, 0x7f, 0xff, 0xff, 0xff, 0xfe, 0x7f,
0x9f, 0xbf, 0xff, 0x0, 0xf0, 0x1c, 0xf3, 0xe7, 0xf8, 0xe, 0x79, 0xff, 0xf3, 0xe6, 0x7e,
0xff, 0xff, 0x3f, 0xff, 0xf3, 0x9e, 0x79, 0xf0, 0xf3, 0xe7, 0xe7, 0xe7, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xe4, 0x30, 0x1f, 0x9f, 0xf0, 0xe, 0x79, 0xff, 0xf9, 0xef, 0x3c, 0xfc, 0xff,
0xff, 0xff, 0xff, 0xfe, 0x7f, 0x80, 0x3f, 0xff, 0x3f, 0xe0, 0x1c, 0xf3, 0xe7, 0xf0, 0xe,
0x79, 0xff, 0xf3, 0xe6, 0x0, 0xff, 0xff, 0x3f, 0xff, 0xf3, 0xde, 0x79, 0xf9, 0xf3, 0xe7,
0xe7, 0xf1, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe4, 0x30, 0x9f, 0x9f, 0xe3, 0xce, 0x79, 0xff,
0xf8, 0x7, 0x3c, 0xf8, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x7f, 0x80, 0x3f, 0xff, 0x3f, 0xc7,
0x9c, 0xf3, 0xe7, 0xe3, 0xce, 0x79, 0xff, 0xf3, 0xe6, 0x0, 0xff, 0xff, 0x3f, 0xff, 0xf0,
0xe, 0x79, 0xf1, 0xf3, 0xe7, 0xe7, 0xfe, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf6, 0x31, 0xbf,
0x9f, 0xe7, 0xce, 0x79, 0xff, 0xf0, 0x7, 0x3c, 0xf2, 0x7f, 0xff, 0xff, 0xff, 0xfe, 0x7f,
0x9f, 0xff, 0xff, 0x3f, 0xcf, 0x9c, 0xf3, 0xe7, 0xe7, 0xce, 0x79, 0xff, 0xf3, 0xe6, 0x7f,
0xff, 0xff, 0x3f, 0xff, 0xe0, 0xe, 0x79, 0xe4, 0xf3, 0xe7, 0xe7, 0xff, 0xbf, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xf2, 0x7f, 0x3f, 0x9f, 0xe7, 0x8e, 0x79, 0xff, 0xf3, 0xe7, 0x38, 0xf3, 0x3f,
0xff, 0xf3, 0xff, 0xfe, 0x7f, 0xcf, 0xbf, 0xff, 0x3f, 0xcf, 0x1c, 0xf3, 0xe7, 0xe7, 0x8e,
0x79, 0xff, 0xf9, 0xc7, 0x3e, 0xff, 0xff, 0x3f, 0xff, 0xe7, 0xce, 0x71, 0xe6, 0x79, 0xc7,
0xe7, 0xef, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf8, 0x7c, 0xff, 0x80, 0x30, 0xe, 0x3, 0xff,
0xf3, 0xf3, 0x0, 0xe7, 0x9f, 0xff, 0xf3, 0xff, 0xfe, 0x0, 0xc0, 0x3f, 0xff, 0x3f, 0xe0,
0x1c, 0x7, 0xf0, 0x70, 0xe, 0x3, 0xff, 0xf8, 0x7, 0x0, 0xff, 0xff, 0x83, 0xff, 0xe7,
0xe6, 0x1, 0xcf, 0x38, 0xf, 0x0, 0xe0, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 0x30, 0xff,
0x80, 0x38, 0x4e, 0x47, 0xff, 0xe7, 0xf3, 0x84, 0xcf, 0x9f, 0xff, 0xf3, 0xff, 0xfe, 0x0,
0xf0, 0x7f, 0xff, 0x3f, 0xf0, 0x9c, 0x8f, 0xf8, 0x78, 0x4e, 0x47, 0xff, 0xfc, 0x27, 0xc1,
0xff, 0xff, 0xc3, 0xff, 0xcf, 0xe7, 0x9, 0x9f, 0x3c, 0x1f, 0x0, 0xf0, 0x7f, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0x3, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff,
 };

const uint8_t pictureB[] PROGMEM = {
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x9f, 0xff, 0xfe, 0x7f, 0xf9,
0xfe, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xff, 0x0, 0xff,
0xfc, 0xff, 0x7, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xe7, 0xff, 0xff, 0xf8, 0x3f, 0xf3, 0xfc,
0x7f, 0xff, 0xff, 0xff, 0xff, 0xe7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0x9f, 0xff, 0xfe, 0x7f, 0xf9, 0xfe, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x7f,
0xff, 0xff, 0xff, 0x0, 0xff, 0xfc, 0xff, 0x7, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xe7, 0xff,
0xff, 0xf8, 0x3f, 0xf3, 0xfc, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xe7, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0x9f, 0xff, 0xfe, 0x7f, 0xf9, 0xfe, 0x1f, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xff, 0x3f, 0xff, 0xfc, 0xff, 0xe7, 0xff, 0xfe,
0x7f, 0xff, 0xff, 0xe7, 0xff, 0xff, 0xff, 0x3f, 0xf3, 0xfc, 0x3f, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xbf, 0x9f, 0xff, 0xfe, 0x7f, 0xf9,
0xfe, 0x9f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xff, 0x3f, 0xff,
0xfc, 0xff, 0xe7, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xe7, 0xff, 0xff, 0xff, 0x3f, 0xf3, 0xfd,
0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x9f,
0x9f, 0xf8, 0x3e, 0x47, 0xf9, 0xfc, 0x9f, 0x3c, 0xcf, 0x9f, 0xff, 0xf3, 0xff, 0xfe, 0x7f,
0xf0, 0xff, 0xff, 0x3f, 0xf0, 0x7c, 0x8f, 0xe7, 0xf8, 0x3e, 0x47, 0xff, 0xfc, 0x27, 0xc3,
0xff, 0xff, 0x3f, 0xf3, 0xf9, 0x3e, 0x79, 0x9f, 0x3c, 0x1f, 0x7, 0xf0, 0x7f, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0x9f, 0x9f, 0xf0, 0x1e, 0x3, 0xff, 0xfc, 0xcf, 0x3c, 0xe7, 0x3f,
0xff, 0xf3, 0xff, 0xfe, 0x7f, 0xc0, 0x7f, 0xff, 0x3f, 0xe0, 0x3c, 0x7, 0xe7, 0xf0, 0x1e,
0x3, 0xff, 0xf8, 0x7, 0x1, 0xff, 0xff, 0x3f, 0xff, 0xf9, 0x9e, 0x79, 0xce, 0x78, 0xf,
0x7, 0xe0, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 0x1f, 0x9f, 0xf7, 0xce, 0x79, 0xff,
0xfd, 0xcf, 0x3c, 0xf3, 0x7f, 0xff, 0xf3, 0xff, 0xfe, 0x7f, 0xcf, 0x3f, 0xff, 0x0, 0xef,
0x9c, 0xf3, 0xe7, 0xf7, 0xce, 0x79, 0xff, 0xf9, 0xc7, 0x3c, 0xff, 0xff, 0x3f, 0xff, 0xfb,
0x9e, 0x79, 0xe6, 0xf9, 0xc7, 0xe7, 0xe7, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf8, 0x1f,
0x9f, 0xf8, 0xe, 0x79, 0xff, 0xf9, 0xcf, 0x3c, 0xf8, 0x7f, 0xff, 0xff, 0xff, 0xfe, 0x7f,
0x9f, 0xbf, 0xff, 0x0, 0xf0, 0x1c, 0xf3, 0xe7, 0xf8, 0xe, 0x79, 0xff, 0xf3, 0xe6, 0x7e,
0xff, 0xff, 0x3f, 0xff, 0xf3, 0x9e, 0x79, 0xf0, 0xf3, 0xe7, 0xe7, 0xe7, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xf0, 0x1f, 0x9f, 0xf0, 0xe, 0x79, 0xff, 0xf9, 0xef, 0x3c, 0xfc, 0xff,
0xff, 0xff, 0xff, 0xfe, 0x7f, 0x80, 0x3f, 0xff, 0x3f, 0xe0, 0x1c, 0xf3, 0xe7, 0xf0, 0xe,
0x79, 0xff, 0xf3, 0xe6, 0x0, 0xff, 0xff, 0x3f, 0xff, 0xf3, 0xde, 0x79, 0xf9, 0xf3, 0xe7,
0xe7, 0xf1, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf0, 0x9f, 0x9f, 0xe3, 0xce, 0x79, 0xff,
0xf8, 0x7, 0x3c, 0xf8, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x7f, 0x80, 0x3f, 0xff, 0x3f, 0xc7,
0x9c, 0xf3, 0xe7, 0xe3, 0xce, 0x79, 0xff, 0xf3, 0xe6, 0x0, 0xff, 0xff, 0x3f, 0xff, 0xf0,
0xe, 0x79, 0xf1, 0xf3, 0xe7, 0xe7, 0xfe, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf1, 0xbf,
0x9f, 0xe7, 0xce, 0x79, 0xff, 0xf0, 0x7, 0x3c, 0xf2, 0x7f, 0xff, 0xff, 0xff, 0xfe, 0x7f,
0x9f, 0xff, 0xff, 0x3f, 0xcf, 0x9c, 0xf3, 0xe7, 0xe7, 0xce, 0x79, 0xff, 0xf3, 0xe6, 0x7f,
0xff, 0xff, 0x3f, 0xff, 0xe0, 0xe, 0x79, 0xe4, 0xf3, 0xe7, 0xe7, 0xff, 0xbf, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0x3f, 0x9f, 0xe7, 0x8e, 0x79, 0xff, 0xf3, 0xe7, 0x38, 0xf3, 0x3f,
0xff, 0xf3, 0xff, 0xfe, 0x7f, 0xcf, 0xbf, 0xff, 0x3f, 0xcf, 0x1c, 0xf3, 0xe7, 0xe7, 0x8e,
0x79, 0xff, 0xf9, 0xc7, 0x3e, 0xff, 0xff, 0x3f, 0xff, 0xe7, 0xce, 0x71, 0xe6, 0x79, 0xc7,
0xe7, 0xef, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 0x7f, 0x80, 0x30, 0xe, 0x3, 0xff,
0xf3, 0xf3, 0x0, 0xe7, 0x9f, 0xff, 0xf3, 0xff, 0xfe, 0x0, 0xc0, 0x3f, 0xff, 0x3f, 0xe0,
0x1c, 0x7, 0xf0, 0x70, 0xe, 0x3, 0xff, 0xf8, 0x7, 0x0, 0xff, 0xff, 0x83, 0xff, 0xe7,
0xe6, 0x1, 0xcf, 0x38, 0xf, 0x0, 0xe0, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf0, 0xff,
0x80, 0x38, 0x4e, 0x47, 0xff, 0xe7, 0xf3, 0x84, 0xcf, 0x9f, 0xff, 0xf3, 0xff, 0xfe, 0x0,
0xf0, 0x7f, 0xff, 0x3f, 0xf0, 0x9c, 0x8f, 0xf8, 0x78, 0x4e, 0x47, 0xff, 0xfc, 0x27, 0xc1,
0xff, 0xff, 0xc3, 0xff, 0xcf, 0xe7, 0x9, 0x9f, 0x3c, 0x1f, 0x0, 0xf0, 0x7f, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xe3, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
0xff, 0xff, 0xff, 0xff, 0xff,
 };


#define PICTURE_WIDTH   640

#define PIN_R1      3
#define PIN_R2      4
#define PIN_G1      5
#define PIN_G2      6
#define PIN_B1      7
#define PIN_B2      8

#define PIN_A       9
#define PIN_B       10

#define PIN_OE      13
#define PIN_STB     12
#define PIN_CLK     11

#define ON_DELAY_US 10

#define OE_ENABLE   0
#define OE_DISABLE  1

#define PANEL_COUNT             8       // number of 16x16 panels
#define PANEL_MX_LINES          4       // number multiplexed lines (2 bits A B = 4 lines)
#define LINES_PER_DATA_PIN      8       // number of lines driven by 1 data pin
#define PIXELS_PER_SEGMENTS     8       // number of contiguuous pixels per segment

#define SCROLL_DIVIDER          2

void setup() {
    pinMode(PIN_R1  , OUTPUT);
    pinMode(PIN_R2  , OUTPUT);
    pinMode(PIN_G1  , OUTPUT);
    pinMode(PIN_G2  , OUTPUT);
    pinMode(PIN_B1  , OUTPUT);
    pinMode(PIN_B2  , OUTPUT);
    pinMode(PIN_A   , OUTPUT);
    pinMode(PIN_B   , OUTPUT);
    pinMode(PIN_OE  , OUTPUT);
    pinMode(PIN_STB , OUTPUT);
    pinMode(PIN_CLK , OUTPUT);
}

uint8_t getSegmentByte(const uint8_t* array, uint16_t pixelIndex)
{
    uint16_t arrayIndex = pixelIndex >> 3;  //quick div by 8
    uint8_t bitIndex = pixelIndex - (arrayIndex << 3);

    uint8_t toReturnValueL = pgm_read_byte(array + arrayIndex)     << bitIndex;
    uint8_t toReturnValueR = pgm_read_byte(array + arrayIndex+1)   >> (8 - bitIndex);
    return ~(toReturnValueL | toReturnValueR);
}

void sendSegmentData(uint16_t pixelIndex, uint16_t line)
{
    pixelIndex += PICTURE_WIDTH * line;

    uint8_t segmentByteR1 = getSegmentByte(pictureR, pixelIndex);
    uint8_t segmentByteG1 = getSegmentByte(pictureG, pixelIndex);
    uint8_t segmentByteB1 = getSegmentByte(pictureB, pixelIndex);

    pixelIndex += (PICTURE_WIDTH << 3);// LINES_PER_DATA_PIN);

    uint8_t segmentByteR2 = getSegmentByte(pictureR, pixelIndex);
    uint8_t segmentByteG2 = getSegmentByte(pictureG, pixelIndex);
    uint8_t segmentByteB2 = getSegmentByte(pictureB, pixelIndex);

    uint8_t i;

    //Drive upper subpanel
    for( i = 0 ; i < PIXELS_PER_SEGMENTS ; i++)
    {
        digitalWrite2(PIN_CLK, 0);

        digitalWrite2(PIN_R1, segmentByteR1 & 0x01);
        digitalWrite2(PIN_G1, segmentByteG1 & 0x01);
        digitalWrite2(PIN_B1, segmentByteB1 & 0x01);

        digitalWrite2(PIN_R2, segmentByteR2 & 0x01);
        digitalWrite2(PIN_G2, segmentByteG2 & 0x01);
        digitalWrite2(PIN_B2, segmentByteB2 & 0x01);

        delayMicroseconds(1);

        digitalWrite2(PIN_CLK, 1);

        segmentByteR1 >>=1;
        segmentByteG1 >>=1;
        segmentByteB1 >>=1;

        segmentByteR2 >>=1;
        segmentByteG2 >>=1;
        segmentByteB2 >>=1;
    }
}

void displayLine(uint8_t lineIndex, uint16_t imagePosition)
{
    //Send Data
    uint8_t i;
    for(i = 0 ; i < PANEL_COUNT * 2 ; i++)
    {
        //uint16_t pixelPosition = imagePosition + PIXELS_PER_SEGMENTS * i;
        sendSegmentData(imagePosition, lineIndex + PANEL_MX_LINES);
        sendSegmentData(imagePosition, lineIndex);
        imagePosition += PIXELS_PER_SEGMENTS;
        digitalWrite(PIN_OE, OE_DISABLE);
    }

    digitalWrite(PIN_OE, OE_DISABLE);

    digitalWrite(PIN_STB, 0);
    digitalWrite(PIN_STB, 1);
    digitalWrite(PIN_STB, 0);

    //Select LED line to light up
    digitalWrite(PIN_A, (lineIndex & 0x01) ? 0x01 : 0x00);
    digitalWrite(PIN_B, (lineIndex & 0x02) ? 0x01 : 0x00);

    digitalWrite(PIN_OE, OE_ENABLE);
    
}

void loop() {
  digitalWrite(PIN_OE, OE_DISABLE);

  while(1)
  {
    uint8_t line;
    static uint16_t scrollPos = 0;
    static uint8_t scrollDivider = 0;

    for(line = 0 ; line < PANEL_MX_LINES ; line ++)
    {
        displayLine(line, scrollPos);
    }

    scrollDivider++;
    if(scrollDivider >= SCROLL_DIVIDER)
    {
        scrollDivider = 0;
        scrollPos++;
        if(scrollPos > (PICTURE_WIDTH - (PIXELS_PER_SEGMENTS * 2 * PANEL_COUNT)))
        {
            scrollPos = 0;
        }
    }
  }
}
